package com.example.beerlist.ui.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.beerlist.InitialActivity;
import com.example.beerlist.R;
import com.example.beerlist.databinding.InitialFragmentBinding;

public class MainFragment extends Fragment {

    private InitialFragmentBinding binding;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.initial_fragment, container, false);


        Button btInventario = view.findViewById(R.id.btInventario);
        btInventario.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext()); //Read Update
                alertDialog.setTitle("Bienvenido");
                alertDialog.setMessage("Cervecerias Kilian");

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getContext(), "Benvenuti alla felicità", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getActivity(), InitialActivity.class);
                        startActivity(i);
                    }

                });
                alertDialog.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getContext(), "Cerrando aplicación.", Toast.LENGTH_SHORT).show();
                        System.exit(0);
                    }
                });

                alertDialog.show();
            }

        });
        return view;
    }
}
