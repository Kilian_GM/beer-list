package com.example.beerlist;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CervezaDao{
    @Query("select * from cerveza")
    LiveData<List<Cerveza>> getCervezas();

    @Insert
    void addCerveza(Cerveza cerveza);

    @Insert
    void addCervezas(List<Cerveza> cervezas);

    @Delete
    void deleteCerveza(Cerveza cerveza);
    @Update
    void updateCerveza(Cerveza cerveza);

    @Query("DELETE FROM cerveza")
    void deleteCervezas();
}
