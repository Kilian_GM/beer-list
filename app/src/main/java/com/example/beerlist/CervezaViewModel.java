package com.example.beerlist;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

public class CervezaViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CervezaDao cervezaDao;
    private MutableLiveData<Boolean> loading;

    public CervezaViewModel(Application application){
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.cervezaDao = appDatabase.getCervezaDao();
    }

    public  LiveData<List<Cerveza>> getCervezas() {
        return cervezaDao.getCervezas();
    }

    public void reload(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }
    public void updateCerveza(Cerveza cerveza){
        cervezaDao.updateCerveza(cerveza);
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Cerveza>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Cerveza> doInBackground(Void... voids) {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );

            CervezaDBAPI api = new CervezaDBAPI();
            ArrayList<Cerveza> result;


            result = api.todasLasCervezas();

            cervezaDao.deleteCervezas();
            cervezaDao.addCervezas(result);

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cerveza> cervezas) {
            super.onPostExecute(cervezas);
            loading.setValue(false);
        }

    }
}
