package com.example.beerlist;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beerlist.databinding.LvCervezaRowBinding;
import com.example.beerlist.databinding.MainFragmentBinding;

import java.util.ArrayList;
import java.util.List;

public class InitialFragment extends Fragment {

    private CervezaViewModel cervezaViewModel;

    private ArrayList<Cerveza> items;
    private CervezasAdapter adapter;
    private MainFragmentBinding binding;
    private ProgressDialog dialog;
    private SharedViewModel sharedModel;
    private boolean mayorEdad = false;
    private LvCervezaRowBinding rowBinding;

    public static InitialFragment newInstance() {
        return new InitialFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();
        adapter = new CervezasAdapter(
                getContext(),
                R.layout.lv_cerveza_row,
                items
        );
        sharedModel = ViewModelProviders.of(getActivity()).get(
                SharedViewModel.class
        );


        binding.lvcervezas.setAdapter(adapter);

        binding.lvcervezas.setOnItemClickListener((adapterView, view12, i, l) -> {
            Cerveza cerveza = (Cerveza) adapterView.getItemAtPosition(i);
            if (!esTablet()) {
                Intent intent = new Intent(getContext(), Details.class);
                intent.putExtra("cerveza", cerveza);
                startActivity(intent);
            } else {
                sharedModel.select(cerveza);
            }
        });

        cervezaViewModel.getCervezas().observe(this, cervezas -> {
            adapter.clear();
            adapter.addAll(cervezas);
        });

        return view;
    }

    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Cerveza>> {
        @Override
        protected ArrayList<Cerveza> doInBackground(Void... voids) {
            CervezaDBAPI api = new CervezaDBAPI();
            ArrayList<Cerveza> result = api.todasLasCervezas();
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cerveza> cervezas) {
            adapter.clear();
            for (Cerveza cerveza : cervezas) {
                adapter.add(cerveza);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cervezas_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");
        cervezaViewModel = ViewModelProviders.of(this).get(CervezaViewModel.class);
        cervezaViewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if (mostrat)
                    dialog.show();
                else
                    dialog.dismiss();
            }
        });
        refresh();
    }

    private void refresh() {
        cervezaViewModel.reload();
    }

}
