package com.example.beerlist;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Cerveza> selected = new MutableLiveData<Cerveza>();

    public void select(Cerveza cerveza) {
        selected.setValue(cerveza);
    }

    public LiveData<Cerveza> getSelected() {
        return selected;
    }
}
