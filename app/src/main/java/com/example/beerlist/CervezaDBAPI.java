package com.example.beerlist;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CervezaDBAPI {
    private final String BASE_URL = "https://api.punkapi.com/v2";

    public ArrayList<Cerveza> todasLasCervezas() {
        Uri builrUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("beers")
                .build();
        String url = builrUri.toString();

        return doCall(url);
    }

    private ArrayList<Cerveza> doCall(String url){
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Cerveza> processJson(String jsonResponse) {
        ArrayList<Cerveza> cervezas = new ArrayList<>();
        try {
            JSONArray jsoncervezas = new JSONArray(jsonResponse);

            for (int i = 0; i < jsoncervezas.length(); i++) {
                JSONObject jsonCerveza = jsoncervezas.getJSONObject(i);

                Cerveza cerveza = new Cerveza();

                cerveza.setDescription(jsonCerveza.getString("description"));
                cerveza.setFirst_brewed(jsonCerveza.getString("first_brewed"));
                cerveza.setImage_url(jsonCerveza.getString("image_url"));
                cerveza.setName(jsonCerveza.getString("name"));
                cerveza.setTagline(jsonCerveza.getString("tagline"));
                cervezas.add(cerveza);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cervezas;
    }
}
