package com.example.beerlist;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.beerlist.databinding.DetailsFragmentBinding;

public class DetailsFragment extends Fragment {
    private View view;

    private DetailsFragmentBinding binding;

    private CervezaViewModel mViewModel;

    public DetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DetailsFragmentBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i != null) {
            Cerveza cerveza = (Cerveza) i.getSerializableExtra("cerveza");

            if (cerveza != null) {
                updateUi(cerveza);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Cerveza>() {
            @Override
            public void onChanged(@Nullable Cerveza cerveza) {
                updateUi(cerveza);
            }
        });

        return view;
    }

    private void updateUi(Cerveza cerveza) {

        binding.tvName.setText(cerveza.getName());
        binding.tvDesciption.setText(cerveza.getDescription());
        binding.tvTagline.setText(cerveza.getTagline());
        binding.tvFirstBrewed.setText(cerveza.getFirst_brewed());

        Glide.with(getContext()).load(
            cerveza.getImage_url()
        ).into(binding.ivPosterImage);
    }

}
