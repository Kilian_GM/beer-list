package com.example.beerlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.beerlist.databinding.LvCervezaRowBinding;

import java.util.List;

public class CervezasAdapter extends ArrayAdapter<Cerveza> {
    private boolean mayorEdad = false;
    public static int cantidad = 0;
    public boolean comprado = false;

    public CervezasAdapter(Context context, int resource, List<Cerveza> objects) {
        super(context, resource, objects);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Cerveza cerveza = getItem(position);

        LvCervezaRowBinding binding = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.lv_cerveza_row, parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        }
        binding.tvcerveza.setText(cerveza.getName());
        Glide.with(getContext()).load(
                cerveza.getImage_url()
        ).apply(new RequestOptions().override(600, 300)).into(binding.ivPosterImage);
        Button btBuy = binding.btBuy;
        btBuy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!mayorEdad) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext()); //Read Update
                    alertDialog.setTitle("Espera...");
                    alertDialog.setMessage("Eres mayor de edad?");

                    alertDialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mayorEdad = true;
                        }

                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getContext(), "No puedes comprar", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                    });

                    alertDialog.show();
                } else {
                    cantidad = cantidad +1;
                }
            }


        });

        return binding.getRoot();
    }

    public int getCantidad() {
        return cantidad;
    }
}
